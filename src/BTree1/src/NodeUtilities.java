package BTree1.src;

/**
 * This class has various utility methods for working with nodes.
 */
public class NodeUtilities {
    static String nodesVisited = null;
    static int sum = 0;
    static int weight = 1;

    /**
     * Inorder traversal of a binary tree rooted at a node.
     *
     * @param btree : The root of the binary tree.
     */
    static public void inorder(Node btree) {
        if (btree != null) {
            nodesVisited = nodesVisited + btree.value + " ";
            inorder(btree.left);
            System.out.print(btree.value + " ");
            inorder(btree.right);
        }
    }

    public static void addNode(Node parent, Node newNode) {
        if (newNode.value < parent.value) {
            if (parent.left == null)
                parent.left = newNode;
            else
                NodeUtilities.addNode(parent.left, newNode);
        } else {
            if (parent.right == null)
                parent.right = newNode;
            else
                NodeUtilities.addNode(parent.right, newNode);

        }
    }

    public static int depthSum(Node root) {
        return depthSum(root, 1);
    }

    private static int depthSum(Node node, int level) {
        if (node == null)
            return 0;

        return level * node.value +
                depthSum(node.left, level + 1) + depthSum(node.right, level + 1);
    }

    public static String ConvertToString(Node root) {
        return toString(root);
    }

    public static String toString(Node root) {
        if (root == null) {
            return "empty";
        } else if (root.left == null && root.right == null) {
            return "" + root.value;
        } else {
            return "(" + root.value + ", " + toString(root.left) + ", " + toString(root.right) + ")";
        }
    }
}