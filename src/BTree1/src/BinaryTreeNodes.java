package BTree1.src;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
   This program demonstrates construction and traversal of binary trees.
 */   
   
public class BinaryTreeNodes
{

    // Creates a binary tree from nodes and traverses it in
    // in inorder.

    public static void main(String[] args) throws IOException {
        Node root = null;  // Will be root of the binary tree.
        Node newNode = null;

        Scanner nodeVal = new Scanner(new File("src/NodeVals.txt"));
        if(nodeVal.hasNext()){
            root = new Node(nodeVal.nextInt());
            System.out.println("Root vale read from file: "+ root.value);
        }
        while (nodeVal.hasNext()){
            newNode = new Node(nodeVal.nextInt());
            System.out.println("Next node value read from file: "+ newNode.value);
            NodeUtilities.addNode(root,newNode);

        }

        NodeUtilities.nodesVisited = "Nodes visited were: ";
        System.out.print("Inorder traversal is: ");
        NodeUtilities.inorder(root);
        System.out.println();
        System.out.println(NodeUtilities.nodesVisited);
        nodeVal.close();
        System.out.println("BinaryTreeNodes Ended");

        // Calling the depthsum method from NodeUtilities
        int depthSum = NodeUtilities.depthSum(root);
        System.out.println("depth sum of the nodes is :"+ depthSum);

        // Calling the toString method from NodeUtilities
        String value = NodeUtilities.ConvertToString(root);
        System.out.println("Binary Tree Converted to String: "+ value);

    }
}

